#!/bin/zsh
setopt extendedglob

include_dir=$XDG_CONFIG_HOME/sway/config.d
config_dir=$XDG_CONFIG_HOME/sway
config_merged=$config_dir/config.merged

cat $include_dir/variables > $config_merged

for include_config in $include_dir/(*~variables); do
	echo "$include_config"
	echo "\n\n### Beginning of $include_config\n" >> $config_merged
	cat $include_config >> $config_merged
done

echo "\ninclude /etc/sway/config.d/*" >> $config_merged

mv -i $config_merged $config_dir/config
